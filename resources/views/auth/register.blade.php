@extends('auth.master') 
@section('content')
<div class="all-wrapper menu-side with-pattern">
	<div class="auth-box-w wider">
		<div class="logo-w">
			<a href="index.html">
				<img alt="" src="img/logo-big.png">
			</a>
		</div>
		<h4 class="auth-header">
			Create new account
		</h4>
		<form class="form-horizontal" method="POST" action="{{ route('register') }}">
			{{ csrf_field() }}
			<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name">Name</label>


				<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
				<div class="pre-icon os-icon os-icon-user-male-circle"></div>
				@if ($errors->has('name'))
				<span class="help-block text-danger">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif

			</div>
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="name">E-mail Address</label>
				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
				<span class="help-block text-danger">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
				<div class="pre-icon os-icon os-icon-email-2-at2"></div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for=""> Password</label>
						<input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
						<div class="pre-icon os-icon os-icon-fingerprint"></div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="">Confirm Password</label>
						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
					</div>
				</div>
			</div>
			<div class="buttons-w">
				<button class="btn btn-primary" type="submit">Register Now</button>
				<a class="btn btn-info" href="{{ route('login') }}">Login</a>
			</div>
		</form>
	</div>
</div>
@endsection