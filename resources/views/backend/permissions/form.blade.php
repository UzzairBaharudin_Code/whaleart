<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">
        Permission Name
    </label>
    <input name="name" type="text" class="form-control" value="{{old('name',$permission->name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    <div class="help-block form-text text-muted form-control-feedback">
        For uniformity please use this structure: 'Verb' 'Model Name' || e.g: Create Post
    </div>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description">
        Permission Description
    </label>
    <textarea name="description" class="form-control"> {{old('description',$permission->description ?? null)}}</textarea>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
</div>
<div class="form-group">
    <label for="">Permission For Which Model</label>
    <select name="dataset_id" id="" class="form-control">
        @foreach($models as $model)
        <option value="{{$model->id}}">Name: {{$model->name}} || Key: {{$model->key}}</option>
        @endforeach
    </select>
</div>