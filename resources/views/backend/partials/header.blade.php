<div class="top-menu-secondary with-overflow color-scheme-dark">	
	<div class="logo-w menu-size">
		<a class="logo" href="{{route('backend.index')}}">
			<img src="{{asset('img/logo.svg')}}">
			<span>KOMENTAR</span>
		</a>
	</div>		
	<div class="top-menu-controls">
		<div class="element-search hidden-lg-down">
			<input placeholder="Start typing to search..." type="text">
		</div>
		<div class="top-icon top-search hidden-xl-up">
			<i class="os-icon os-icon-ui-37"></i>
		</div>
			
		<div class="logged-user-w">
			<div class="logged-user-i">
				<div class="avatar-w">
					<img alt="" width="139" height="139" src="{{asset(Auth::user()->profile->avatar)}}">
				</div>
				<div class="logged-user-menu">
					<div class="logged-user-avatar-info">
						<div class="avatar-w">
							<img alt="" src="{{asset(Auth::user()->profile->avatar)}}">
						</div>
						<div class="logged-user-info-w">
							<div class="logged-user-name">
								{{Auth::user()->name}}								
							</div>
							<div class="logged-user-role">
								{{Auth::user()->roles->first()->name}}
							</div>
						</div>
					</div>
					<div class="bg-icon">
						<i class="os-icon os-icon-wallet-loaded"></i>
					</div>
					<ul>						
						<li>
							<a href="{{route('users.myaccount')}}">
								<i class="os-icon os-icon-user-male-circle2"></i>
								<span>My Account</span>
							</a>
						</li>
						<li>
							<a href="" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								<i class="os-icon os-icon-signs-11"></i>
								<span>Logout</span>
							</a>

							<form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
	</div>
</div>