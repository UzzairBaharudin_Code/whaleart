<!--START - Mobile Menu-->
        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
          <div class="mm-logo-buttons-w">
            <a class="mm-logo" href="#"><img src="{{asset('img/logo.svg')}}"> <span>KOMENTAR</span></a>
            <div class="mm-buttons">
              <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
              </div>
              <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
              </div>
            </div>
          </div>
          <div class="menu-and-user">
            <div class="logged-user-w">
              <div class="avatar-w">
                <img alt="" src="{{asset(Auth::user()->profile->avatar)}}">
              </div>
              <div class="logged-user-info-w">
                <div class="logged-user-name">
                 {{Auth::user()->name}}
                </div>
                <div class="logged-user-role">
                  {{Auth::user()->roles->first()->name}}
                </div>
              </div>
            </div>
            <!--START - Mobile Menu List-->
            <ul class="main-menu">
              <li class="">
                <a href="{{route('posts.all')}}">
                  <div class="icon-w">
                    <div class="os-icon os-icon-pencil-12"></div>
                  </div>
                  <span>All Posts</span>
                </a>                
              </li>
              @if(Auth::user()->isAdmin())
              {{--  // Admin Manage Posts  --}}
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-pencil-12"></div>
                  </div>
                  <span>Post</span></a>
                <ul class="sub-menu">
                  <li>
                  <a href="{{route('posts.index')}}">All Posts</a>
                    </li>
                    <li>
                      <a href="{{route('posts.create')}}">Add New Posts</a>
                    </li>                 
                </ul>
              </li>
              {{--  // Admin Manage Users  --}}
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle"></div>
                  </div>
                  <span>Users</span></a>
                <ul class="sub-menu">
                  <li>
                      <a href="{{route('users.index')}}">All Users</a>
                    </li>
                    <li>
                      <a href="{{route('users.create')}}">Add New Users</a>
                    </li>                
                </ul>
              </li>
              {{--  // Admin Manage Roles & Permission  --}}
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-tasks-checked"></div>
                  </div>
                  <span>Roles & Permission</span></a>
                <ul class="sub-menu">
                  <li>
                      <a href="{{route('roles.index')}}">All Roles</a>
                    </li>
                    <li>
                      <a href="{{route('roles.create')}}">Add New Role</a>
                    </li>
                    <li>
                      <a href="{{route('permissions.index')}}">All Permissions</a>
                    </li>                
                </ul>
              </li>
              {{--  // Admin Manage Models  --}}
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-coins-4"></div>
                  </div>
                  <span>Models</span></a>
                <ul class="sub-menu">
                  <li>
                      <a href="{{route('models.index')}}">All Models</a>
                    </li>
                    <li>
                      <a href="{{route('models.create')}}">Add New Model</a>
                    </li>               
                </ul>
              </li>
              @endif
            </ul>
            <!--
            END - Mobile Menu List
            -->            
          </div>
        </div>
        <!--
        END - Mobile Menu
        -->