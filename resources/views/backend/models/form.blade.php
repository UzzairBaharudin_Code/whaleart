<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name">
        Model Name
    </label>
    <input name="name" type="text" class="form-control" value="{{old('name',$dataset->name ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description">
        Model Description
    </label>
    <input name="description" type="text" class="form-control" value="{{old('description',$dataset->description ?? null)}}">
    <span class="help-block text-danger">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
</div>
