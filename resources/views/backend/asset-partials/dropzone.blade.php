@section('dropzone-css')
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
@stop

@section('dropzone-js')
<script src="{{ asset('js/dropzone.js') }}"></script>
@stop