@section('datatables-css')
<link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@stop

@section('datatables-js')
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script>
    if ($('#datatable').length) {
    $('#datatable').DataTable(
      { 
        buttons: ['copy', 'excel', 'pdf'], 
        pageLength:5,
        lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
        "columnDefs": [ {
"targets": 0,
"orderable": false
} ]
    });
  }
    </script>
@stop