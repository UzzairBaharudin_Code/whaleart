@section('fullcalendar-css')
<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">
@stop

@section('fullcalendar-js')
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
@stop