@section('todo-js')
<script>
// #18. TODO Application

// Tasks foldable trigger
$('.tasks-header-toggler').on('click', function () {
  $(this).closest('.tasks-section').find('.tasks-list-w').slideToggle(100);
  return false;
});

// Sidebar Sections foldable trigger
$('.todo-sidebar-section-toggle').on('click', function () {
  $(this).closest('.todo-sidebar-section').find('.todo-sidebar-section-contents').slideToggle(100);
  return false;
});

// Sidebar Sub Sections foldable trigger
$('.todo-sidebar-section-sub-section-toggler').on('click', function () {
  $(this).closest('.todo-sidebar-section-sub-section').find('.todo-sidebar-section-sub-section-content').slideToggle(100);
  return false;
});

// Drag init
if ($('.tasks-list').length) {
  // INIT DRAG AND DROP FOR Todo Tasks
  var dragulaTasksObj = dragula($('.tasks-list').toArray(), {
    moves: function moves(el, container, handle) {
      return handle.classList.contains('drag-handle');
    }
  }).on('drag', function () {}).on('drop', function (el) {}).on('over', function (el, container) {
    $(container).closest('.tasks-list').addClass('over');
  }).on('out', function (el, container, source) {

    var new_pipeline_body = $(container).closest('.tasks-list');
    new_pipeline_body.removeClass('over');
    var old_pipeline_body = $(source).closest('.tasks-list');
  });
}
// Task actions init

// Complete/Done
$('.task-btn-done').on('click', function () {
  $(this).closest('.draggable-task').toggleClass('complete');
  return false;
});

// Favorite/star
$('.task-btn-star').on('click', function () {
  $(this).closest('.draggable-task').toggleClass('favorite');
  return false;
});

// Delete
var timeoutDeleteTask;
$('.task-btn-delete').on('click', function () {
  if (confirm('Are you sure you want to delete this task?')) {
    var $task_to_remove = $(this).closest('.draggable-task');
    $task_to_remove.addClass('pre-removed');
    $task_to_remove.append('<a href="#" class="task-btn-undelete">Undo Delete</a>');
    timeoutDeleteTask = setTimeout(function () {
      $task_to_remove.slideUp(300, function () {
        $(this).remove();
      });
    }, 5000);
  }
  return false;
});

$('.tasks-list').on('click', '.task-btn-undelete', function () {
  $(this).closest('.draggable-task').removeClass('pre-removed');
  $(this).remove();
  if (typeof timeoutDeleteTask !== 'undefined') {
    clearTimeout(timeoutDeleteTask);
  }
  return false;
});
</script>
@stop