@section('daterangepicker-css')
<link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">
@stop

@section('daterangepicker-js')
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script>
$('input.single-daterange').daterangepicker({ "singleDatePicker": true });
$('input.multi-daterange').daterangepicker({ "startDate": "03/28/2017", "endDate": "04/06/2017" });
</script>
@stop