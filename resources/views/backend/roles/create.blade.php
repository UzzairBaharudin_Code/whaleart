@extends('backend.master') @section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Add New Role
      </h6>
      <div class="element-box">
        <form action="{{route('roles.store')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}} 
          @include('backend.roles.form')
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Save
            </button>
            <a href="{{route('roles.index')}}" class="btn btn-info">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop