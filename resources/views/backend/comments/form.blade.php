<div class="form-group">
    <label for="description">
        Author
    </label>
    <p>{{$comment->user->name}}</p>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description">
        Description
    </label>
    <textarea name="description" class="form-control"> {{old('description',$comment->description ?? null)}}</textarea>
    <span class="help-block text-danger">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
</div>
