@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Comments
			</h6>
			<form class="form" action="{{route('comments.massdelete')}}" method="post">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			<div class="element-box">
			<div class="element-box-content">			
			<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="bulkdelete" value="bulkdelete"  onclick="return confirm('Are you sure you want to delete the selected record(s)?')"><i class="os-icon os-icon-ui-15"></i> Delete</button>
			</div>
			</div>
			</div>
			<div class="element-box">
				<div class="table-responsive">
					<table id="datatable" class="table table-striped">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" class="checked" {{$comments->count() > 0 ? '':'disabled'}} /></th>
								<th> # </th>	
								<th>Author</th>							
								<th>Description</th>								
								<th>Post</th>	
															                             
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($comments as $key=>$comment)
							@if($comment->user->id == Auth::id())
							<tr>
								<td><input type="checkbox" name="id[]" value="{{$comment->id}}" class="check-all"></td>
								<td>{{++$key}}</td>
								<td>{{$comment->user['name']}}</td>				
								<td>{{$comment->description}}</td>								
								<td><a class="" href="posts/{{$comment->post->post_slug}}" target="_blank">{{$comment->post->post_title}}</a></td>                             
								<td class="row-actions">
									<a href="{{route('comments.edit',['id'=>$comment->id])}}">
										<i class="os-icon os-icon-pencil-2"></i>
									</a>			
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection
@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')