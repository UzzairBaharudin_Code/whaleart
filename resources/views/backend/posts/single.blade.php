{{--  @extends('backend.master') @section('content')
<div class="row">
    <div class="col-md-6 offset-md-3">
        <img class="img-fluid rounded" src="{{asset($post->post_featuredimg)}}" alt="">
        <h3 class="mt-3">{{$post->post_title}}</h3>
        <p class="content text-justify">
            {!! $post->post_content !!}
        </p>
        <p>
            <b>Posted by</b> {{$post->user->name}} | {{$post->created_at->toDayDateTimeString()}}</p>
        <hr>
        <form method="POST" action="{{route('comments.save', ['id'=> $post->id] )}}">
            {{ csrf_field() }}
            <div class="media">
                <img class="mr-3" src="{{asset(Auth::user()->profile->avatar)}}" width="50px">
                <div class="media-body">
                    <h5 class="mt-0">{{Auth::user()->name}}</h5>
                    <div class="form-group">
                        <textarea name="description" placeholder="Say something?" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add comment</button>
                    </div>
                </div>
            </div>
        </form>
        <hr> @foreach($post->comments as $comment)
        <div class="media">
            <img class="mr-3" src="{{asset($comment->user->profile->avatar)}}" width="70px">
            <div class="media-body">
                <h5 class="mt-0">{{$comment->user->name}}
                    <small>
                        <i>
                            <b>posted on</b> {{ $comment->created_at->toDayDateTimeString()}}</i>
                    </small>
                </h5>
                <div class="comment-editable">
                <div class="comment-description">
                    {!! $comment->description !!}
                </div>
                <br> 
                @if($comment->user->id == Auth::id())
                
                <form method="POST" href="{{route('comments.kill', ['id'=>$comment->id])}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                <input type="submit" class="btn btn-link" value="Edit">   
                <input type="submit" class="btn btn-link" value="Delete" />                        
                </form>  
                @endif
            </div>
        </div>
    </div>
    <br> @endforeach
</div>
</div>

@endsection
  --}}
