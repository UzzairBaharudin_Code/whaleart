
					<div class="row">
						<div class="col-md-6">
						<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
						<label for=""> Title</label>
						<input name="post_title" type="text" class="form-control" value="{{old('post_title',$post->post_title ?? null)}}">
            @if ($errors->has('post_title'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('post_title') }}</strong>
						</span>
						@endif
					</div>										
					<div class="form-group{{ $errors->has('post_content') ? ' has-error' : '' }}">
						<label for="">
							Content
						</label>
						<textarea class="form-control" name="post_content" id="content" cols="30" rows="10">{{old('post_content',$post->post_content ?? null)}}</textarea>
            @if ($errors->has('post_content'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('post_content') }}</strong>
						</span>
						@endif
					</div>
						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('post_featuredimg') ? ' has-error' : '' }}">
						<label for=""> Featured Image</label>
						<br>
						<input name="post_featuredimg" type="file" class="">
						@if(!empty($post->post_featuredimg))
						<img class="img-responsive img-thumbnail mx-auto d-block" src="{{$post->post_featuredimg}}">
						<p class="badge badge-info mt-2">File path: {{$post->post_featuredimg}}</p>
						@else
						<img src="" alt="" src="{{asset('img/photo.svg')}}">
						@endif												
            @if ($errors->has('post_featuredimg'))
						<span class="help-block text-danger">
							<strong>{{ $errors->first('post_featuredimg') }}</strong>
						</span>
						@endif
					</div>
						</div>
					</div>
