@extends('backend.master') @section('content')

<div class="element-wrapper">
<h6 class="element-header">
				All Posts
            </h6>
<div class="row">              
            @foreach($posts as $post)
            <div class="col-md-4 col-sm-12 col-lg-4 mt-2">
                <div class="card" style="">
                    <img class="card-img-top" data-src="" alt="100%x180" style="height: 180px; width: 100%; display: block;" src="{{$post['post_featuredimg']}}"
                        data-holder-rendered="true">
                    <div class="card-body">
                        <h5 class="card-title">{{str_limit($post['post_title'],$limit=44,$end='...')}}</h5>
                        <p class="card-text">{!! str_limit($post['post_content'], $limit = 150, $end = '...') !!}</p>
                        <a href="{{$post['post_slug']}}" class="btn btn-primary">Read More...</a>
                    </div>
                </div>
            </div>

            @endforeach
    
</div>
</div>
@endsection