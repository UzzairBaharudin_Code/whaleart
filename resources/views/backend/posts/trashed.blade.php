@extends('backend.master')
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Trashed Posts
			</h6>
			<form action="" method="post">
			{{csrf_field()}}
			<div class="element-box sticky-top">
			<div class="element-box-content">				
				<div class="btn-group">
				<button  class="btn btn-success" id="restore" type="submit" name="restore" value="restore"><i class="os-icon os-icon-common-07"></i> Restore</button>
				</div>
				<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="shred" value="shred"  onclick="return confirm('Are you sure you want to permanently delete the record(s)?')"><i class="os-icon os-icon-ui-15"></i> Permanently Delete</button>
					</div>
			</div>
			</div>
			<div class="element-box">
				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
							<th>
							
							<input type="checkbox" id="selectall" class="checked" {{$posts->count() > 0 ? '':'disabled'}}/>
							
							</th>
								<th>#</th>
								<th>Image</th>
								<th>Post Name</th>								
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $key=>$post)
							<tr>
							<td><input type="checkbox"  name="id[]" 
								value="{{$post->id}}" class="check-all"></td>
								<td>{{++$key}}</td>
								<td>
									<img src="{{$post->post_featuredimg}}" class="img-responsive" width="100px" />
								</td>
								<td>{{$post->post_title}}</td>								
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection

@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')
