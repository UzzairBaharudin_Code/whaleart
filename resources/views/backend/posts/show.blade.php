@extends('backend.master')
@section('content')
@if($posts->count() >1)
<div class="row">
	<div class="col-sm-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				All Posts
			</h6>
			<form class="form" action="{{route('posts.massdelete')}}" method="post">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			<div class="element-box">
			<div class="element-box-content">
			<div class="btn-group">
				<a href="{{route('posts.create')}}" class="btn btn-primary">Add New Post</a>
			</div>
			<div class="btn-group">
				<button  class="btn btn-danger" id="bulk-delete" type="submit" name="bulkdelete" value="bulkdelete"  onclick="return confirm('Are you sure you want to move the selected record(s) to trash?')"><i class="os-icon os-icon-ui-15"></i> Move To Trash</button>
			</div>
			</div>
			</div>
			<div class="element-box">

				<div class="table-responsive">
					<table id="datatable" class="table table-striped ">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" class="checked" {{$posts->count() > 0 ? '':'disabled'}} /></th>
								<th>#</th>
								<th>Featured Image</th>
								<th>Post Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($posts as $key=>$post)
							<tr>
								<td><input type="checkbox" name="id[]"
								value="{{$post->id}}" class="check-all"></td>
								<td>{{++$key}}</td>
								<td>
									<img src="{{$post->post_featuredimg}}" class="img-responsive" width="100px" />
								</td>
								<td>{{$post->post_title}}</td>
								<td class="row-actions">
									<a href="{{route('posts.edit',['id'=>$post->id])}}">
										<i class="os-icon os-icon-pencil-2"></i> Edit
									</a>														
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
@else
	<div class="text-center">
<img src="{{asset('img/create.svg')}}" alt="" width="300px" style="padding: 20px 0"/>
<h4>There is no Post. Click here to add.</h4>
</div>
@endif
@endsection

@include('backend.asset-partials.datatables')
@include('backend.asset-partials.form-function')
