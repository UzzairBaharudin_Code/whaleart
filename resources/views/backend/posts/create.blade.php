@extends('backend.master') 
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="element-wrapper">
			<h6 class="element-header">
				Create New Post
			</h6>
			<form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
			<div class="element-box">										
						<button class="btn btn-primary" type="submit" name="saveback"> Save Post</button>
                        <a class="btn btn-info" href="{{route('posts.index')}}"> Cancel</a>				
			</div>
			<div class="element-box">				
					{{csrf_field()}}
                    @include('backend.posts.form')									
			</div>
			</form>
		</div>
	</div>
</div>
@stop


@section('ckeditor-js')
 <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@stop