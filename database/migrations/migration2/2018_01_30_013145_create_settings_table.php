<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_name');
            $table->string('site_email');
            $table->string('site_phone');
            $table->string('site_fax');
            $table->string('site_address_1');
            $table->string('site_address_2');
            $table->integer('site_postcode');
            $table->string('site_city');
            $table->string('site_state');
            $table->string('site_country');
            $table->string('site_facebook')->nullable();
            $table->string('site_youtube')->nullable();
            $table->string('site_twitter')->nullable();
            $table->string('site_instagram')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
