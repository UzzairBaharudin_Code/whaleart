<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_title')->comment('Post title');
            $table->string('post_slug')->comment('Post slug');
            $table->text('post_content')->comment('Post content');
            $table->string('post_featuredimg')->default('/uploads/post/default-post.png')->comment('Featured image of the post');
            $table->integer('post_status')->default(1)->comment('Post status, 1=active, 0=inactive/archived');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
