<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create(['name'=>'administrator','key'=>str_slug('administrator'),'description'=>'Administrator role']);
        Role::create(['name' => 'author', 'key'=>str_slug('author'),'description' => 'Author role']);

    }
}
