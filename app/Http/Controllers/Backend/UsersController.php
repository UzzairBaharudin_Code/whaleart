<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\UsersRequest;
use App\Repositories\Backend\UsersRepo;
use App\Repositories\Backend\ProfilesRepo;
use App\Repositories\Backend\RolesRepo;

class UsersController extends Controller
{    

    public function index(UsersRepo $users)
    {
        $users = $users->all();
        return view('backend.users.index')->with(compact('users'));        
    }

    public function create(RolesRepo $roles)
    {
        $role_ids = [];
        $roles = $roles->all();
        return view('backend.users.create')
        ->with(compact('role_ids','roles'));        
    }

    public function store(UsersRepo $user, ProfilesRepo $profile, UsersRequest $request)
    {
        // Create user and its profile
        $user = $user->create($request);
        $user->roles()->attach($request->roles);
        //create profile too
        // If avatar field is filled in
        if ($request->hasFile('avatar')) {
            $this->validate($request,[
                'avatar' => 'image',
            ]);
            $avatar = $request->avatar;
            $avatar_new_name = time() . $avatar->getClientOriginalName();
            $avatar->move('uploads/avatars', $avatar_new_name);            
        }
        $profile = $profile->create($request, $user, $avatar_new_name);                         
        Session::flash('success', 'User was created successfully');
        return redirect()->route('users.index');

    }

    public function edit(UsersRepo $user,RolesRepo $roles,$id)
    {   
        $user = $user->find($id);
        $roles = $roles->all();
        $role_ids = [];
        foreach($user->roles as $user_role){
            $role_ids[] = $user_role->id;            
        }
        return view('backend.users.edit')
        ->with(compact('user','roles','role_ids'));
    }

    public function update(UsersRepo $userrepo, UsersRequest $request, $id)
    {
        $user = $userrepo->find($id);
        $userrepo->update($user, $request);
        Session::flash('success', 'The user has been updated successfully');
        return redirect()->back();
    }

    public function massdelete(UsersRepo $users,Request $request)
    {   
        $users->massdelete($request);

        Session::flash('success', 'The user(s) was successfully deleted');
        return redirect()->route('users.index');
    }  

    public function myaccount()
    {
        return view('backend.users.profile')
            ->with('user', Auth::user());
    }
}
