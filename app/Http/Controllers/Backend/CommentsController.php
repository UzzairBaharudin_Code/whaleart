<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Comment;
use App\Models\Backend\Post;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\Backend\CommentsRequest;

class CommentsController extends Controller
{
    public function index(){
        return view('backend.comments.index')
        ->with('comments', Comment::all());
    }

    public function save(CommentsRequest $request,Post $post)
    {        
        $post->saveComment(
            request('description')
        );
        
        Session::flash('success', 'Your comment has been posted successfully');
        return redirect()->back();
    }

    public function edit($id){
        return view('backend.comments.edit')
        ->with('comment', Comment::find($id));        
    }

    public function update(CommentsRequest $request, $id){
        
        Comment::find($id)->update([
            'description' => $request->description,            
        ]);
        Session::flash('success', 'The comment was successfully updated');
        return redirect()->back();
    }

    public function massdelete(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Comment::find($id)->delete();
        }
        Session::flash('success', 'The comment(s) was successfully deleted');
        return redirect()->route('comments.index');
    }  
    
    public function kill(Post $post){
        $post->deleteComment(request('id'));
        Session::flash('success', 'The comment(s) was successfully deleted');
        return redirect()->route('comments.index');
    }
}
