<?php

namespace App\Http\Controllers\Backend;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Backend\Profile;
use Session;
use App\Models\User;

class ProfilesController extends Controller
{
    public function update(Request $request)
    {
        // Fetch current user object
        $user = Auth::user();

        // Validate name,email field - email for each user should be unique
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        
        // If avatar fied is filled in
        if ($request->hasFile('avatar')) {
            $avatar = $request->avatar;
            $avatar_new_name = time() . $avatar->getClientOriginalName();
            $avatar->move('uploads/avatars', $avatar_new_name);
            $user->profile->avatar = 'uploads/avatars/' . $avatar_new_name;
            // attact to the current user and save
            $user->profile->save();
        }

        //if password field is not empty
        if (!empty($request->password)) {
            $this->validate($request, [
                'password' => 'required|alpha_num|min:8|confirmed',
            ]);
            $user->password = bcrypt($request->password);
            $user->save();
            $user->profile->save();
            Auth::logout();
            Session::flash('success', 'You have changed your password, please log in again with your newly created password');            
        }

        //Save for User Model and Profile Model. Check one-to-one relationship
        $user->save();
        $user->profile->save();
        
        Session::flash('success', 'Profile was updated successfully');
        return redirect()->back();

    }
}
