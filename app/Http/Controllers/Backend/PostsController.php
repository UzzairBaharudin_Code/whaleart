<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PostsRequest;
use DB;
use Auth;
use App\Models\Backend\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Carbon\Carbon;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.posts.index')
        ->with('posts', Post::all());
    }

    public function allposts(){
        return view('backend.posts.all')
        ->with('posts', Post::all());
    }

    public function singlePost($post_slug){
        $post = Post::where('post_slug', $post_slug)->first();
        return view('backend.posts.single')->with('post', $post);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('backend.posts.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
        
        $this->validate($request, [
            'post_featuredimg' => 'image',
        ]);
        // Start saving the image to /public/uploads/posts
        $featured = $request->file('post_featuredimg');
        $featured_new_name = time() . $featured->getClientOriginalName();
        $featured->move('uploads/post/', $featured_new_name);
        // Create new post
        $post = Post::create([
            'post_title' => $request->post_title,
            'post_slug' => str_slug($request->post_title),
            'post_content' => $request->post_content,
            'post_featuredimg' => '/uploads/post/' . $featured_new_name,
            'user_id' => Auth::id()
        ]);       

        // $post->post_categories()->attach($request->postcats);
        Session::flash('success', 'The post was succesfully saved');
        return redirect()->route('posts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {               
        $post = Post::find($id);        
        return view('backend.posts.edit')
            ->with('post', $post);            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, $id)
    {
            $post = Post::find($id);
            $post->post_title = $request->post_title;
            $post->post_slug = str_slug($request->post_title);
            $post->post_content = $request->post_content;            
        
        if ($request->hasFile('post_featuredimg')) {
            $this->validate($request, [
                'featured' => 'image',
            ]);
            $featured = $request->file('post_featuredimg');
            $featured_new_name = time() . $featured->getClientOriginalName();
            $featured->move('uploads/posts', $featured_new_name);
            $post->post_featuredimg = '/uploads/posts/'.$featured_new_name;            
        }
            $post->save();
        Session::flash('success', 'The post was successfully update');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function massdelete(Request $request)
    {   
        $checked = $request->input('id', []);
        foreach($checked as $id){
            Post::find($id)->delete();
        }        
        Session::flash('success', 'The post was successfully deleted');
        return redirect()->route('posts.index');
    }  

    

    /**
     * Display a listing of the trashed posts.
     *
     * @return \Illuminate\Http\Response
     */

    public function trashed()
    {
        $posts = Post::onlyTrashed()->get();
        return view('backend.posts.trashed')->with('posts', $posts);

    }

    public function shred(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Post::onlyTrashed()->where('id', $id)->forceDelete();
        }
        Session::flash('success', 'The post was successfully deleted');
        return redirect()->route('posts.trashed');

    }

    public function bulkdelete(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Post::where('id', $id)->delete();
        }
        Session::flash('success', 'The post was successfully deleted');
        return redirect()->back();

    }

    public function restore(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Post::onlyTrashed()->where('id', $id)->restore();
        }
        Session::flash('success', 'The post was successfully restored');
        return redirect()->back();

    }
}
