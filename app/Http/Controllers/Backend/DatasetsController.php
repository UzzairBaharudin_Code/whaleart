<?php

namespace App\Http\Controllers\Backend;

use App\Models\Backend\Dataset;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DatasetsRequest;
use Illuminate\Http\Request;
use Session;

class DatasetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.models.index')->with('datasets', Dataset::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.models.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DatasetsRequest $request)
    {
        Dataset::create([
            'name' => $request->name,
            'key' => str_slug($request->name),
            'description' => $request->description,
        ]);
        Session::flash('success', 'The model has been created');
        return redirect()->route('models.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.models.edit')->with('dataset', Dataset::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DatasetsRequest $request, $id)
    {
        Dataset::find($id)->update([
            'name' => $request->name,
            'key' => str_slug($request->name),
            'description' => $request->description,
        ]);
        Session::flash('success', 'The model has been updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function massdelete(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Dataset::find($id)->delete();
        }
        Session::flash('success', 'The dataset(s) was successfully deleted');
        return redirect()->route('models.index');
    }
}
