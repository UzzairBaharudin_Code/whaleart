<?php

namespace App\Http\Controllers\Backend;

use App\Models\Backend\Dataset;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PermissionsRequest;
use App\Models\Permission;
use Illuminate\Http\Request;
use Session;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.permissions.index')->with('permissions', Permission::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.permissions.create')->with('models', Dataset::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionsRequest $request)
    {
        Permission::create([
            'name' => ucwords($request->name),
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'dataset_id' => $request->dataset_id
        ]);
        Session::flash('success', 'The permission was created successfully');
        return redirect()->route('permissions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.permissions.edit')
        ->with('permission', Permission::find($id))
        ->with('models', Dataset::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionsRequest $request, $id)
    {
        Permission::find($id)->update([
            'name' => ucwords($request->name),
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'dataset_id' => $request->dataset_id
        ]);
        Session::flash('success', 'The permission has been updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function massdelete(Request $request)
    {
        $checked = $request->input('id', []);
        foreach ($checked as $id) {
            Permission::find($id)->delete();
        }
        Session::flash('success', 'The permission(s) was successfully deleted');
        return redirect()->route('permissions.index');
    }
}
