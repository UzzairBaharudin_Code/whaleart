<?php

namespace App\Http\Middleware;

use Closure;
use App\PostCat;
use Session;
class PostsAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
         
    public function handle($request, Closure $next)
    {
        $postcats = PostCat::all();
        if($postcats->count()<1){
            Session::flash('warning','There is no Post Category, please add one to add a post');
            return redirect()->back();
        }
        return $next($request);
    }
}
