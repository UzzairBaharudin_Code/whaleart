<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_name' => 'required',
            'site_email' => 'required|email',
            'site_phone' => 'required',
            'site_fax' => 'required',
            'site_address_1' => 'required|min:5',
            'site_address_2' => 'required|min:5',
            'site_postcode' => 'required',
            'site_city' => 'required',
            'site_state' => 'required',
            'site_country' => 'required',
        ];
    }
}
