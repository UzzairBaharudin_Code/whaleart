<?php 

namespace App\Repositories\Backend;

use App\Models\User;
use App\Models\Role;
use App\Models\Profile;
use Validator;

class UsersRepo{

    public function all(){
        return User::all();
    }

    public function find($id){
        
        return User::find($id);
    }
    
    public function create($request){
    
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);                     
    }

    public function update($user, $request){
      
        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile->first_name = $request->first_name;
        $user->profile->last_name = $request->last_name;

        if (!empty($request->password)) {
           Validator::make($request->all(), [
                'password' => 'required|alpha_num|min:8|confirmed',
            ]);
            $user->password = bcrypt($request->password);
        }

        if ($request->hasFile('avatar')) {            
            Validator::make($request->all(),[
                'avatar' => 'image',
            ]);
            $avatar = $request->avatar;
            $avatar_new_name = time() . $avatar->getClientOriginalName();
            $avatar->move('uploads/avatars', $avatar_new_name);
            $user->profile->avatar = '/uploads/avatars/' . $avatar_new_name;
        }
        $user->save();
        $user->profile->save();    

    }

    public function massdelete($request){
        $checked = $request->input('id', []);
        foreach($checked as $id){
            User::find($id)->delete();
        }
    }
}