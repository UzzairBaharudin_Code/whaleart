<?php 

namespace App\Repositories\Backend;

use App\Models\Profile;

class ProfilesRepo{
    
    public function create($request, $user, $avatar_new_name){
                
        return Profile::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/' . $avatar_new_name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name
        ]);
    }
}