<?php

namespace App\Observers;

use App\Models\User;
use Mail;

class UserModelObserver{

    public function creating(User $user){

        $user->name = strtoupper($user->name);

    }
}