<?php

namespace App\Observers\Backend;
use App\Models\Backend\Dataset;


class ModelObserver{

    public function creating(Dataset $dataset){
        $dataset->key = str_slug($dataset->name);
    }
}