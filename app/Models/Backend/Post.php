<?php

namespace App\Models\Backend;

use App\Models\Backend\Comment;
use Auth;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function saveComment($description)
    {
        Comment::create([
            'description' => $description,
            'post_id' => $this->id,
            'user_id' => Auth::id()
        ]);
    } 
    
    public function deleteComment($id){
        Comment::find($id)->delete();
    }
}
