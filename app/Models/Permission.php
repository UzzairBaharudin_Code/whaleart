<?php

namespace App\Models;

use App\Models\Backend\Dataset;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = [];

    public function dataset()
    {
        return $this->belongsTo(Dataset::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
