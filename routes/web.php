<?php

use Illuminate\Support\Facades\Input;

/*******************************************************************************************
 *                                  AUTH
 *******************************************************************************************/

Auth::routes();

/*******************************************************************************************
 *                                  BACKEND
 *******************************************************************************************/

Route::group(['prefix' => 'backend', 'middleware' => 'backend', 'namespace' => 'Backend'], function () {
    // Dashboard
    Route::get('/', ['uses' => 'BackendsController@index', 'as' => 'backend.index']);

    // Posts
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/trashed', ['uses' => 'PostsController@trashed', 'as' => 'posts.trashed']);
        Route::get('/shred/{id}', ['uses' => 'PostsController@shred', 'as' => 'posts.shred']);
        Route::delete('/massdelete', ['uses' => 'PostsController@massdelete', 'as' => 'posts.massdelete']);
        Route::get('/all', ['uses' => 'PostsController@allposts', 'as' => 'posts.all']);
    });

    Route::resource('posts', 'PostsController', ['except' => ['show', 'destroy']]);

    // Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/myaccount', ['uses' => 'UsersController@myaccount', 'as' => 'users.myaccount']);
        Route::post('/profile/update', ['uses' => 'ProfilesController@update', 'as' => 'profile.update']);
        Route::delete('/massdelete', ['uses' => 'UsersController@massdelete', 'as' => 'users.massdelete']);
    });
    Route::resource('users', 'UsersController');

    // Roles
    Route::group(['prefix' => 'roles'], function () {
        Route::delete('/massdelete', ['uses' => 'RolesController@massdelete', 'as' => 'roles.massdelete']);
    });
    Route::resource('roles', 'RolesController');

    // Permissions
    Route::group(['prefix' => 'permissions'], function () {
        Route::delete('/massdelete', ['uses' => 'PermissionsController@massdelete', 'as' => 'permissions.massdelete']);
    });
    Route::resource('permissions', 'PermissionsController');

    // Models
    Route::group(['prefix' => 'models'], function () {
        Route::delete('/massdelete', ['uses' => 'DatasetsController@massdelete', 'as' => 'models.massdelete']);

    });
    Route::resource('models', 'DatasetsController');

    // Menu Management Page
    Route::get('/menu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        return view('backend.menus.menu', ['menus' => $menu]);
    });

    // Menu Manager
    Route::get('/managemenu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        return view('backend.menus.menuManage', ['menus' => $menu]);
    });

    // Menu Sorter Function
    Route::get('/order-menu', function () {
        $menu = DB::table('tbl_menus')->orderBy('order', 'ASC')->get();
        $itemID = Input::get('itemID');
        $itemIndex = Input::get('itemIndex');

        foreach ($menu as $value) {
            return DB::table('tbl_menus')
                ->where('menu_id', '=', $itemID)
                ->update(array('order' => $itemIndex));
        }
        return redirect()->route('backend.index');
    });

    // Comments
    Route::group(['prefix' => 'comments'], function () {
        Route::delete('/massdelete', ['uses' => 'CommentsController@massdelete', 'as' => 'comments.massdelete']);
    });
    Route::resource('comments', 'CommentsController', ['except' => ['show', 'store', 'destroy']]);

    // Email

    Route::post('/send/email', ['uses'=> 'EmailController@send', 'as'=> 'emails.send']);
});

Route::get('/', ['uses' => 'HomeController@index']);
